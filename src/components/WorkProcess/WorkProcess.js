import React from 'react';

import './WorkProcess.sass';

const WorkProcess = () => {
    const startVideo = () => {
        document.querySelector('#youtube-video').src = "https://www.youtube.com/embed/F9T8j_NFXr0?autoplay=1";
        document.querySelector('.custom-player-button').className += ' hide';
        setTimeout(() => {
            document.querySelector('.custom-player-button').style = 'display: none';
        }, 300)
    };
    return(
        <section className="s-work-process" id={"work-process"}>
            <h2>Our Work Process</h2>
            <p>Was years it seasons was there form he in in them together over that, 
            <br/>third sixth gathered female creeping bearing behold years.</p>
            <div className="container">
                <div id="custom-video-player">
                    <iframe id={"youtube-video"} width="100%" height="663" src="" frameBorder="0"
                            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                            allowFullScreen/>
                    <div className={"custom-player-button"} onClick={() => {startVideo()}}>
                        <i className="fas fa-play" />
                    </div>
                </div>
            </div>
        </section>
    )
}

export default WorkProcess;
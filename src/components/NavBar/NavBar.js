import React from 'react';

import './NavBar.sass';

import logo from '../../imgs/logo.png'

const NavBar = () => {
	let int = setInterval(() => {
		if (window.pageYOffset > 0){
			document.querySelector('.s-header').classList = 's-header st';
			clearInterval(int);
		}
	},500);

    return(
        <React.Fragment>
			<section className="s-header">
            <div className="container">
				<header className="header">
					<div className="logo">
						{/*Надо бы href делать на главную, но это лендинг -_- и я хз что сюда писать*/}
						<h1><a href="#opening"><img src={logo} /></a></h1>
					</div>
					<nav>
						<ul className="nav-list">
							<li>
								<a href="#opening">Home</a>
							</li>
							<li>
								<a href="#about">About</a>
							</li>
							<li>
								<a href="#work">Work</a>
							</li>
							<li>
								<a href="#work-process">Process</a>
							</li>
							<li>
								<a href="#services">Services</a>
							</li>
							<li>
								<a href="#quote">Clients</a>
							</li>
							<li>
								<a href="#contacts">Contacts</a>
							</li>
						</ul>
					</nav>
				</header>
			</div>
			</section>
        </React.Fragment>
    )
}

export default NavBar;

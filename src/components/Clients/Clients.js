import React, {useState, useEffect} from "react";

import './Clients.sass';

import Client1 from '../../imgs/client1.png';
import Client2 from '../../imgs/Client2.png';
import Client3 from '../../imgs/Client3.png';
import Client4 from '../../imgs/Client4.png';

const Clients = () => {
    const [step, setSteps] = useState(0);

    useEffect(()=>{
        let interval;
        if (laps() > 0){
            interval = setInterval(() => {
                animateClients(laps());
            },2000)
        }

        return () => {
            clearInterval(interval);
        }
    });

    const laps = () => {
        let steps = document.querySelectorAll('.clients-item').length - 5;
        return steps;
    };

    const animateClients = (maxStep) => {
        if (step <= maxStep){
            setSteps(step + 1);
            document.querySelector('.clients-container').style = `transform: translate(-${step * 20}%, 0)`;
        } else if (step > maxStep){
            setSteps(1);
            document.querySelector('.clients-container').style = `transform: translate(0, 0)`;
        }
    };

    return(
        <section className={"s-clients"} id={"clients"}>
            <div className="container">
                <div className="clients-container">
                    <div className="clients-item">
                        <img src={Client1} alt=""/>
                    </div>
                    <div className="clients-item">
                        <img src={Client2} alt=""/>
                    </div>
                    <div className="clients-item">
                        <img src={Client3} alt=""/>
                    </div>
                    <div className="clients-item">
                        <img src={Client4} alt=""/>
                    </div>
                    <div className="clients-item">
                        <img src={Client1} alt=""/>
                    </div>
                    <div className="clients-item">
                        <img src={Client2} alt=""/>
                    </div>
                    <div className="clients-item">
                        <img src={Client3} alt=""/>
                    </div>
                    <div className="clients-item">
                        <img src={Client4} alt=""/>
                    </div>
                </div>
            </div>
        </section>
    )
};

export default Clients;
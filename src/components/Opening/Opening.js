import React from 'react'
import PropTypes from 'prop-types'

import './Opening.sass';

const Opening = (props) => {
	return (
		<section className="s-opening" id={"opening"}>
			<div className="container opening">
				<div className="opening-info">
					<h2>We Dising and Develop</h2>
					<p>We are a new design studio based in USA. We have over
					20 years of combined experience, and know a thing or two
					about designing websites and mobile apps.</p>
					<a href="#" className="btn-primary">Contact us</a>
				</div>
			</div>
		</section>
	)
}

export default Opening;

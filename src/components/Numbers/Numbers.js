import React, {useEffect} from 'react'

import './Numbers.sass';

const Numbers = () => {
    useEffect(() => {
        let int = setInterval(() => {
            if (window.pageYOffset + document.documentElement.clientHeight/2 >= document.querySelector('.s-numbers').offsetTop){
                document.querySelectorAll('.numbers-item').forEach(el => {
                    setValue(el, el.dataset.target, 3, 5)
                })
                clearInterval(int);
            }
        }, 1000);
    });

    const setValue = (elem, value, shift, speed) => {
        let counter = elem.querySelector('.counter');
        let interval = setInterval(function(){
            if (counter.innerHTML*1+shift >= value) {
                counter.innerHTML = value;
                clearInterval(interval);
            } else {
                counter.innerHTML = counter.innerHTML*1+shift;
            }
        }, speed);
    };
	return (
		<section className="s-numbers">
			<div className="container">
                <div className="numbers-item" data-target="548">
                    <i className="fas fa-briefcase"></i>
                    <div>
                        <p className="counter">0</p>
                        <p>Projects Complited</p>
                    </div>
                </div>
                <div className="numbers-item" data-target="1465">
                    <i className="far fa-clock"></i>
                    <div>
                        <p className="counter">0</p>
                        <p>Working Hours</p>
                    </div>
                </div>
                <div className="numbers-item" data-target="612">
                    <i className="far fa-star"></i>
                    <div>
                        <p className="counter">0</p>
                        <p>Positive Feedbacks</p>
                    </div>
                </div>
                <div className="numbers-item" data-target="735">
                    <i className="far fa-heart"></i>
                    <div>
                        <p className="counter">0</p>
                        <p>Happy Clients</p>
                    </div>
                </div>
			</div>
		</section>
	)
}

export default Numbers;

import React from "react";

import './Services.sass';

const Services = () => {
    return (
        <section className={"s-services"} id={"services"}>
            <div className="container">
                <div>
                    <i className="far fa-gem"/>
                    <h4>UI/UX Design</h4>
                    <p>Be set fourth land god darkness
                        make it wherein own</p>
                </div>
                <div>
                    <i className="fas fa-code"/>
                    <h4>Web Development</h4>
                    <p>A she'd them bring void moving
                        third she'd kind fill</p>
                </div>
                <div>
                    <i className="fas fa-mobile-alt"/>
                    <h4>App / Mobile</h4>
                    <p>Dominion man second spirit he,
                        earth they're creeping</p>
                </div>
                <div>
                    <i className="fas fa-gamepad"/>
                    <h4>Game Design</h4>
                    <p>Morning his saying moveth it
                        multiply appear life be</p>
                </div>
                <div>
                    <i className="fas fa-plane-departure"/>
                    <h4>SEO / Marketing</h4>
                    <p>Give won't after land fill creeping
                        meat you, may</p>
                </div>
                <div>
                    <i className="far fa-star"/>
                    <h4>Photography</h4>
                    <p>Creepeth one seas cattle grass
                        give moving saw give</p>
                </div>
                <div>
                    <i className="fas fa-magic"/>
                    <h4>Graphic Design</h4>
                    <p>Open, great whales air rule for,
                        fourth life whales</p>
                </div>
                <div>
                    <i className="fas fa-fill-drip"/>
                    <h4>Illustrations</h4>
                    <p>Whales likeness hath, man kind
                        for them air two won't</p>
                </div>

            </div>
        </section>
    );
};

export default Services;
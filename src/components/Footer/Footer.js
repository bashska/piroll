import React from "react";

import './Footer.sass';

const Footer = () => {
    return(
        <footer>
            <div className="container">
                <div>
                    <span>Piroll Design, Inc.</span>
                    <p>© 2017 Piroll. All rights reserved. <br/>
                        Designed by robirurk.</p>
                </div>
                <div>
                    <p>hello@pirolltheme.com</p>
                    <br/>
                    <p>+44 987 065 908 </p>
                </div>
                <div className={"footer-nav"}>
                    <div>
                        <p><a href="#work">Projects</a></p>
                        <p><a href="#about">About</a></p>
                        <p><a href="#services">Services</a></p>
                        <p><a href="#">Carreer</a></p>
                    </div>
                    <div>
                        <p><a href="#">News</a></p>
                        <p><a href="#">Events</a></p>
                        <p><a href="#contacts">Contact</a></p>
                        <p><a href="#">Legals</a></p>
                    </div>
                    <div>
                        <div>
                            <a href="http://www.facebook.com" target='_blank'><i className="fab fa-facebook-square"/></a>
                            <a href="http://www.twitter.com" target='_blank'><i className="fab fa-twitter-square"/></a>
                        </div>
                        <div>
                            <a href="http://www.instagramm.com" target='_blank'><i className="fab fa-instagram"/></a>
                            <a href="http://www.pinterest.com" target='_blank'><i className="fab fa-pinterest-square"/></a>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    )
};

export default Footer;
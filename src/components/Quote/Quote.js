import React from "react";

import './Quote.sass';

const Quote = () => {
    //Это страшный код, я пишу его после 30 часов бодрствования, простите те, кто будет это читать
    //Async (magick)
    setTimeout(() => {
        setNums();
        setControl();
    }, 0);

    //Ставим отступы для элементов, ибо двигаться будет их родитель-контейнер
    const setNums = () => {
        let quotes = document.querySelectorAll('.quote-item');
        quotes.forEach((el, key) => {
            el.style = `margin-left: ${key * 100}%`
        })
    };
    //Делаем точки управления
    const setControl = () => {
        let quotes = document.querySelectorAll('.quote-item');
        quotes.forEach((el, key) => {
            document.querySelector('.s-quote .controls').innerHTML += `<span class="control-item ${key == 0 && "active"}" data-count=${key}></span>`;
        });

        document.querySelectorAll('.control-item').forEach(el => {
            el.addEventListener("click", () => {
                toggleControl(el);
            })
        })

    };
    //Ну тут ничего особенного, как и выше...
    const toggleControl = (el) => {
        let t = el.dataset.count;
        let controls = document.querySelectorAll('.s-quote .control-item');

        document.querySelector('.quote-container').style = `transform: translate(${-(t * 1170)}px,0)`;

        controls.forEach(el => {
            el.classList = 'control-item';
        });

        controls[t].classList = 'control-item active';
    };

    return (
        <section className={"s-quote"} id={"quote"}>
            <div className="container">
                <div className="quote-container">
                    <div className={"quote-item"}>
                        <p>
                            “ Outstanding job and exceeded all expectations. It was a pleasure
                            to work with them on a sizable first project and am looking
                            forward to start the next one asap. ”
                        </p>
                        <span>Michael Hopkins</span>
                    </div>
                    <div className={"quote-item"}>
                        <p>
                            “ Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium ad consectetur consequuntur corporis culpa deserunt, doloremque doloribus, eaque eius ex excepturi fugit hic inventore ipsa ipsum iste itaque iure iusto laudantium maiores minima modi molestias nulla numquam obcaecati, odit perferendis praesentium quasi quidem quisquam rem soluta tempora totam ullam vel voluptates voluptatum. Debitis dolores ea esse eum eveniet explicabo iste minima, modi nobis omnis quas quia repellendus soluta, velit vitae? ”
                        </p>
                        <span>Lorem 70</span>
                    </div>
                    <div className={"quote-item"}>
                        <p>
                            “ The show must go on,
                            Inside my heart is breaking,
                            My make-up may be flaking,
                            But my smile still stays on. ”
                        </p>
                        <span>Freddie Mercury</span>
                    </div>
                </div>
                <div className="controls">

                </div>
            </div>
        </section>
    );
};

export default Quote;
import React, {useEffect} from 'react'

import './Skills.sass'
//Не нашел лучше, на PSD нет картинок...
import desk from '../../imgs/desk.jpeg';

const Skills = () => {
	useEffect(() => {
		let int = setInterval(() => {
			if (window.pageYOffset + document.documentElement.clientHeight/2 >= document.querySelector('.s-skills').offsetTop){
				document.querySelectorAll('.counterUp-container').forEach(el => {
					setValue(el, el.dataset.target, 1, (100/el.dataset.target)*30)
				});
				clearInterval(int);
			}
		}, 1000);

		return () => {
			clearInterval(int);
		}
	});

	const setValue = (elem, value, shift, speed) => {
		let counter = elem.querySelector('span span.num');
		let diapason = elem.querySelector('.counterUp >div');
		console.log(diapason);
		let interval = setInterval(function(){
			if (counter.innerHTML*1+shift >= value) {
				counter.innerHTML = value;
				diapason.style.width = `${value}%`;
				clearInterval(interval);
			} else {
				counter.innerHTML = counter.innerHTML*1+shift;
				diapason.style.width = `${counter.innerHTML*1+shift}%`;
			}
		}, speed);
	};

	return (
		<section className="s-skills">
			<div className="container">
				<div>
					<h2>Professional Skills</h2>
					<div className="counterUp-container" data-target="75">
						<span>UI/UX Desing <span className="num">0</span></span>
						<div className="counterUp">
							<div></div>
						</div>
					</div>
					<div className="counterUp-container" data-target="90">
						<span>WEB Development <span className="num">0</span></span>
						<div className="counterUp" >
							<div></div>
						</div>
					</div>
					<div className="counterUp-container" data-target="65">
						<span>Marketing <span className="num">0</span></span>
						<div className="counterUp" >
							<div></div>
						</div>
					</div>
				</div>
				<div>
					<img src={desk} alt=""/>
				</div>
			</div>
		</section>
	)
}

export default Skills

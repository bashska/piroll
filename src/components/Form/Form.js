import React from "react";

import './Form.sass';

const Form = () => {
    const submit = () => {
        //Заготовка под промисс
        !validation() && console.log(validation())
    };

    const validation = () => {
        let name = document.querySelector('.input-name');
        let email = document.querySelector('.input-email');
        let title = document.querySelector('.input-title');
        let comment = document.querySelector('.input-comment');

        !name.value ? name.classList = 'input-name danger' : name.classList = 'input-name success';
        !validateEmail(email.value) ? email.classList = 'input-email danger' : email.classList = 'input-email success';
        !title.value ? title.classList = 'input-title danger' : title.classList = 'input-title success';
        !comment.value ? comment.classList = 'input-comment danger' : comment.classList = 'input-comment success';

        return document.querySelector('.danger');
    };

    const validateEmail = (email) => {
        let re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    };

    return(
        <section className={"s-form"} id={"contacts"}>
            <h2>Need a Project?</h2>
            <p>Let us know what you're looking for in an agency. We'll take a look and see
                <br/>if this could be the start of something beautiful.</p>
            <div className="container">
                <form action="">
                    <div className={"row"}>
                        <input placeholder={"Your Name"} className={"input-name"}/>
                        <input placeholder={"Your Email"} className={"input-email"}/>
                    </div>
                    <input placeholder={"Your Title"} className={"input-title"}/>
                    <input placeholder={"Your Comment"} className={"input-comment"}/>
                </form>
                <div className={"btn-primary"} onClick={()=>{submit()}}>
                    Send Message
                </div>
            </div>
        </section>
    )
};

export default Form;
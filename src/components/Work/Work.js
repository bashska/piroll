import React, {useState, useEffect} from 'react'
import PropTypes from 'prop-types'

import './Work.sass';
import { isNull } from 'util';

const Work = ({workItems}) => {
    const [showFullWorks, showFullWorksToggler] = useState(false)
    const [root, setRoot] = useState(null);

    const closeWorkItems = () => {
        document.querySelectorAll('.work-item.new').forEach(e => {
            e.className += ' toClose'
        })
    }

    const openModal = (k) => {
        setRoot(workItems[k]);
    }
    const closeModal = () => {
        setRoot(null);
    }
    const nextModal = () => {
        setTimeout(() => {
            workItems.indexOf(root) + 1 < workItems.length ? setRoot(workItems[workItems.indexOf(root) + 1]) : null
        }, 300)
        workItems.indexOf(root) + 1 < workItems.length ? document.querySelector('.modal').className += ' next' : null;
        console.log(document.querySelector('.modal'))
        setTimeout(() => {
            document.querySelector('.modal').classList = 'modal visible';
        }, 600)
    }
    const prevModal = () => {
        setTimeout(() => {
            workItems.indexOf(root) - 1 >= 0 ? setRoot(workItems[workItems.indexOf(root) - 1]) : null
        }, 300)
        workItems.indexOf(root) - 1 >= 0 ? document.querySelector('.modal').className += ' prev' : null;
        setTimeout(() => {
            document.querySelector('.modal').classList = 'modal visible';
        }, 600)
    }
    //↥↥↥ По-хорошему это надо вынести в контейнер, но логика прискорбная (надо бы делать что то в виде пагинатора, а тут он просто загружает остальные элементы) и мне лень перекидывать все через пропсы, поэтому я забил... 
	return (
		<section className="s-work" id={"work"}>
                {workItems.map((i, k) => {
                    return( 
                        k < 8 ?
                        <div className="work-item" key={k} onClick={() => {openModal(k)}}>
                            <img src={i.img} />
                            <div className="popUp"><i className="far fa-eye"></i></div>
                        </div> :
                        showFullWorks &&
                        <div className="work-item new" key={k} onClick={() => {openModal(k)}}>
                            <img src={i.img} />
                            <div className="popUp"><i className="far fa-eye"></i></div>
                        </div>
                    )
                })}
                <div className="btn-all-works" onClick={() => {showFullWorks ? closeWorkItems() & setTimeout(() => {showFullWorksToggler(!showFullWorks)}, 600) : showFullWorksToggler(!showFullWorks)}}>
                    <span>{showFullWorks ? 'Hide' : 'Load more works'}</span>
                </div>
                <div className={`modal ${!isNull(root) ? 'visible' : ''}`}>
                    {root && 
                        <React.Fragment>

                        <div className="main-content">
                            <div className="description">
                                <div>
                                    <h2>{root.title}</h2>
                                    <p>{root.description}</p>
                                    <div className="info">
                                        <span><h4>Client: </h4><p>{root.client}</p></span>
                                        <span><h4>Date: </h4><p>{root.date.toLocaleString('de-DE')}</p></span>
                                        <span>
                                            <a href="http://www.facebook.com" target='_blank'><i className="fab fa-facebook-square"></i></a>
                                            <a href="http://www.twitter.com" target='_blank'><i className="fab fa-twitter-square"></i></a>
                                            <a href="http://www.instagramm.com" target='_blank'><i className="fab fa-instagram"></i></a>
                                            <a href="http://www.pinterest.com" target='_blank'><i className="fab fa-pinterest-square"></i></a>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <img src={root.img} />
                            </div>
                        </div>
                        <div className="modal-control">
                            <div className="prev" onClick={() => {prevModal()}}>
                                <div><i className="fas fa-chevron-left"></i><span>Previous Project</span></div>
                            </div>
                            <div className="close">
                                <i className="fas fa-times-circle" onClick={() => {closeModal()}}></i>
                            </div>
                            <div className="next" onClick={() => {nextModal()}}>
                                <div><span>Next Project</span><i className="fas fa-chevron-right"></i></div>
                            </div>
                        </div>
                        </React.Fragment>
                        
                    }
                </div>
                
		</section>
	)
}

export default Work;

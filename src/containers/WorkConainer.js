import React, {useEffect} from 'react'
import PropTypes from 'prop-types'

import Work from '../components/Work/Work';
const WorkContainer = (props) => {
    const workItems = [
        {uniqId: 1, title: 'Light Breakfast', description: '...', client: 'Emma Moris', date: new Date, img: 'https://images.pexels.com/photos/2792186/pexels-photo-2792186.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260'},
        {uniqId: 2, title: 'Pink Water', description: '...', client: 'Moris Emma', date: new Date, img: 'https://images.pexels.com/photos/2693200/pexels-photo-2693200.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260'},
        {uniqId: 3, title: 'Cactus', description: '...', client: 'Emma Moris', date: new Date, img: 'https://images.pexels.com/photos/1022921/pexels-photo-1022921.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260'},
        {uniqId: 4, title: 'Night sky', description: '...', client: 'Emma Moris', date: new Date, img: 'https://images.pexels.com/photos/2469122/pexels-photo-2469122.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260'},
        {uniqId: 5, title: 'Sleep Street', description: '...', client: 'Emma Moris', date: new Date, img: 'https://images.pexels.com/photos/2879991/pexels-photo-2879991.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260'},
        {uniqId: 6, title: 'Another One Bites The Dust', description: '...', client: 'Queen', date: new Date, img: 'https://images.pexels.com/photos/566641/pexels-photo-566641.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260'},
        {uniqId: 7, title: 'Megaladon', description: '...', client: 'Emma Moris', date: new Date, img: 'https://images.pexels.com/photos/2170473/pexels-photo-2170473.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260'},
        {uniqId: 8, title: 'Kawasaki', description: '...', client: 'Emma Moris', date: new Date, img: 'https://images.pexels.com/photos/2747545/pexels-photo-2747545.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500'},
        {uniqId: 9, title: 'Neon', description: '...', client: 'Emma Moris', date: new Date, img: 'https://images.pexels.com/photos/1580625/pexels-photo-1580625.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260'},
        {uniqId: 10, title: 'City', description: '...', client: 'Emma Moris', date: new Date, img: 'https://images.pexels.com/photos/1823743/pexels-photo-1823743.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260'},
        {uniqId: 11, title: 'Light Lamp', description: '...', client: 'Emma Moris', date: new Date, img: 'https://images.pexels.com/photos/1166643/pexels-photo-1166643.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260'},
        {uniqId: 12, title: 'Siberia', description: '...', client: 'Emma Moris', date: new Date, img: 'https://images.pexels.com/photos/1366919/pexels-photo-1366919.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260'},
    ];
    workItems.forEach(e => {
        e.description = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.';
    })
	return (
        <Work workItems={workItems} />
	)
}

export default WorkContainer;
